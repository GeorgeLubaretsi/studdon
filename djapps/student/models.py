from datetime import date

from django.contrib.postgres.fields import JSONField
from django.db import models
from django.db.models import Case, ExpressionWrapper, F, Q, When
from django.db.models.functions import ExtractDay, ExtractMonth, ExtractYear
from django.utils.translation import ugettext_lazy as _
from faker import Factory


class StudentQueryset(models.Manager):
    """
    Overriding Student model manager's QS in order to allow querying by age
    """

    def get_queryset(self):
        today = date.today()

        # Django does not support conditional expressions on annotated
        # fields in the same iteration, so two annotations are required
        return super(StudentQueryset, self).get_queryset().annotate(
            birth_month=ExtractMonth('date_of_birth'),
            birth_day=ExtractDay('date_of_birth'),
            # Probable because it assumes that object had the birthday in this year
            probable_age=ExpressionWrapper(
                today.year - ExtractYear('date_of_birth'),
                output_field=models.IntegerField(),
            )
        ).annotate(
            # Why this instead of timedelta?
            # Timedelta does not take into account leap years.
            # This solution works on leap days by comparing current month and the day and just
            # subtracting 1 from the probable age if necessary. Also, works with Django queries!
            age=Case(
                When(
                    Q(birth_month__gt=today.month) |
                    Q(birth_month=today.month, birth_day__gt=today.day),
                    then=F('probable_age') - 1,
                ),
                default=F('probable_age'),
                output_field=models.IntegerField()
            )
        )


class Student(models.Model):
    DEFAULT_DEGREE = 'AS'
    DEGREE_CHOICES = (
        (DEFAULT_DEGREE, _('Associate')),
        ('BS', _('Bachelor')),
        ('MA', _('Master'))
    )

    first_name = models.CharField(_('first name'), max_length=60)
    last_name = models.CharField(_('last name'), max_length=60)
    email_address = models.EmailField(_('email address'), max_length=255)
    date_of_birth = models.DateField(_('date of birth'))
    address = models.CharField(_('address'), max_length=255)
    city = models.CharField(_('city'), max_length=255)
    degree = models.CharField(_('degree'), max_length=2, choices=DEGREE_CHOICES, default=DEFAULT_DEGREE)
    user_provided = JSONField(_('user_provided'), help_text=_('User provided parameters'), default=dict, blank=True,
                              null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    objects = StudentQueryset()

    def __str__(self):
        return self.full_name

    class Meta:
        ordering = ('-created_at',)

    @property
    def full_name(self):
        return '{0} {1}'.format(self.first_name, self.last_name)

    @classmethod
    def generate_random_data(cls, amount=500):
        fake = Factory.create('ka_GE')
        fake_en = Factory.create('en_US')
        objects_to_create = []
        for _ in range(amount):
            student = cls()
            student.first_name = fake.first_name()
            student.last_name = fake.last_name()
            student.email_address = fake_en.free_email()
            student.date_of_birth = fake.date_object()
            student.address = fake.street_address()
            student.city = fake.city()
            objects_to_create.append(student)

        cls.objects.bulk_create(objects_to_create)
