from rest_framework import serializers

from djapps.student import models
from .filters import StudentFilterSet


class StudentSerializer(serializers.ModelSerializer):
    user_provided = serializers.JSONField(default=dict)
    age = serializers.IntegerField()

    class Meta:
        model = models.Student
        fields = (
            'id', 'first_name', 'last_name', 'email_address', 'date_of_birth', 'address', 'city', 'degree',
            'user_provided', 'created_at', 'modified_at', 'full_name', 'age'
        )
        read_only_fields = ('created_at', 'modified_at', 'full_name', 'age')
        filter_class = StudentFilterSet

