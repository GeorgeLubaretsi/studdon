from rest_framework import viewsets

from djapps.student.api.filters import StudentFilterSet
from djapps.student.api.serializers import StudentSerializer
from djapps.student.models import Student


class StudentViewSet(viewsets.ModelViewSet):
    serializer_class = StudentSerializer
    queryset = Student.objects.all()
    filter_class = StudentFilterSet
