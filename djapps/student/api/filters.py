from django_filters.rest_framework import FilterSet, NumberFilter
from django.utils.translation import ugettext_lazy as _

from ..models import Student


class StudentFilterSet(FilterSet):
    age = NumberFilter('age', _('Age'))

    class Meta:
        model = Student
        fields = ['last_name', 'age']
