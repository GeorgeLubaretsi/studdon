from django.contrib import admin

from djapps.student import models


class StudentAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Student, StudentAdmin)
