from django.core.management.base import BaseCommand
from django.utils.translation import ugettext_lazy as _

from djapps.student.models import Student


class Command(BaseCommand):
    help = _('Generates random student data')

    def handle(self, *args, **options):
        Student.generate_random_data(amount=1000)
