from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers

from djapps.student.api import views as student_views

router = routers.DefaultRouter()
router.register(r'students', student_views.StudentViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
]
