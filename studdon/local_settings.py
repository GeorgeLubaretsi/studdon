import os
import dj_database_url
from decouple import config

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG = config('DEBUG', default=False, cast=bool)

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')

ALLOWED_HOSTS = config('DJANGO_ALLOWED_HOSTS').split(',')

DATABASES = {
    'default': dj_database_url.config(
        default=config('DATABASE_URL')
    )
}

SECRET_KEY = config('SECRET_KEY')

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'
