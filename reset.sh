#!/usr/bin/env bash
echo 'DROP SCHEMA public CASCADE; CREATE SCHEMA public;' | python manage.py dbshell
python manage.py migrate
python manage.py generate_random_data
